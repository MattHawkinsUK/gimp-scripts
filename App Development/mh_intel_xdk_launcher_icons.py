#!/usr/bin/env python

# GIMP Plugin - Intel XDK App Launcher Icons Generator
# Copyright (c) 2016 Matt Hawkins
# http://www.tech-spy.co.uk/

# This is Python-fu plugin for Gimp.
# It takes the current image and creates Launcher icons
# for use within the Intel XDK. You can specify a background
# colour if your source image has transparency.

# The original icon is scaled so you should start with a nice
# high quality original. A good start is a 512x512 or bigger PNG.

from gimpfu import *
import os

def python_intel_xdk_launcher_icons(image,tdrawable,platform,background_enable,background_color,outputFolder):
  g = gimp.pdb

  g.gimp_message_set_handler( ERROR_CONSOLE )

  # Define list of pixel sizes to create
  if platform==0:
    # Android sizes
    sizes = [36,48,72,96,512]
  elif platform==1:
    # iOS sizes
    sizes = [29,40,50,57,58,60,72,76,80,87,100,114,120,144,152,180]  
  elif platform==2:
    # Windows 10 sizes
    sizes = [24,30,33,44,50,58,62,70,71,99,106,120,150,170,210,300,360]
  elif platform==3:
    # Chrome sizes
    sizes = [16,128]
  else:
    sizes = [16]
  
  # Set foreground and background colours
  g.gimp_context_set_background(background_color)
  g.gimp_context_set_foreground((255,255,255))

  for size in sizes:
    target_width=size
    target_height=size

    generateImage(g,tdrawable,target_width,target_height,background_enable,outputFolder)

def generateImage(g,tdrawable,target_width,target_height,background_enable,outputFolder):

  imageX = g.gimp_image_new(target_width,target_height,0)

  # Create background layer
  if (background_enable==1):
    opacity=100
  else:
    opacity=0
  
  back_layer = g.gimp_layer_new(imageX,target_width, target_height, 0, "Background Layer", opacity, 0)
  g.gimp_drawable_fill(back_layer, 1)
  g.gimp_image_insert_layer(imageX, back_layer, None, 0)
  g.gimp_layer_set_offsets(back_layer, 0, 0)

  display = g.gimp_display_new(imageX)
  fileextension = ".png"  

  icon_layer = g.gimp_layer_new_from_drawable(tdrawable, imageX)
  g.gimp_image_insert_layer(imageX, icon_layer, None, 0)
  g.gimp_layer_scale(icon_layer, target_width, target_height,False)

  
  # Merge and save
  temp_layer = g.gimp_image_merge_visible_layers(imageX, 1)
  filename = "launcher_" + str(target_width) + "x" + str(target_height) + fileextension
  filename = os.path.join(outputFolder,filename)
  g.file_png_save(imageX, temp_layer, filename, "raw_filename", 0, 9, 0, 0, 0, 0, 0)  
  
  #      (PF_RADIO, "platform", "Platform:", 0,(("Android",1),("iOS",0),("Windows 10",0))),
register(
  "python_intel_xdk_launcher_icons",
  "Intel XDK App Launcher Icons Generator",
  "Generate a set of app launcher icons for use with the Intel XDK.",
  "Matt Hawkins",
  "Matt Hawkins",
  "2016",
  "<Image>/Filters/TechSpy/_App Launcher Icons",
  "RGB*,GRAY*",
  [
      (PF_OPTION, "platform", "Platform:", 0,["Android","iOS","Windows 10","Chrome"]),  
      (PF_TOGGLE, "background_enable", "Background:", True),
      (PF_COLOR,  "background_color", "Background colour", (0, 0, 0)    ),
      (PF_DIRNAME, "outputFolder", "Output directory", "/")
  ],
  [],
  python_intel_xdk_launcher_icons)

main()
