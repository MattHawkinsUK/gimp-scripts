#!/usr/bin/env python

# GIMP Plugin - Intel XDK App Splash Screen Generator
# Copyright (c) 2016 Matt Hawkins
# http://www.tech-spy.co.uk/

# This is Python-fu plugin for Gimp.
# It takes the current image and creates Splash screens
# for use within the Intel XDK. There are three sets
# of image sizes defined.

# You can specify a background colour if your source image has transparency.

from gimpfu import *
import os

def python_intel_xdk_splash(image,tdrawable,platform,iconscale,background_enable,background_color,outputFolder):
  g = gimp.pdb
  g.gimp_message_set_handler( ERROR_CONSOLE )
  
  # Define list of pixel sizes to create
  if platform==0:
    # Android sizes
    sizes = [960,720],[640,480],[470,320],[426,320],[720,960],[480,640],[320,470],[320,426]
  elif platform==1:
    # iOS sizes
    sizes = [750,1334],[1242,2208],[640,1136],[640,960],[320,480],[1536,2048],[768,1024],[1334,750],[2208,1242],[2048,1536],[1024,768]
  elif platform==2:
    # Windows 10 sizes
    sizes = [310,150],[434,210],[744,360],[620,300],[480,800],[672,1120],[1152,1920]
  else:
    sizes = [640,480]

  if tdrawable.width!=tdrawable.height:
    raise RuntimeError("Your source image is not square. Height and width must match!")

  # Set foreground and background colours
  g.gimp_context_set_background(background_color)   
  g.gimp_context_set_foreground((255,255,255))  
  
  for size in sizes:    
    # Get size of splash image
    target_width=size[0]
    target_height=size[1]
    
    # Determine size of icon we need to fit this splash image
    if target_width>target_height:
      icon_size = target_height
    else:
      icon_size = target_width
    icon_size = icon_size * (iconscale/100)
    
    generateImage(g,tdrawable,icon_size,target_width,target_height,background_enable,outputFolder)    
    
def generateImage(g,tdrawable,icon_size,target_width,target_height,background_enable,outputFolder):

  imageX = g.gimp_image_new(target_width,target_height,0)  
  
  # Create background layer
 
  if (background_enable==1):
    opacity=100
  else:
    opacity=0
    
  back_layer = g.gimp_layer_new(imageX,target_width, target_height, 0, "Background Layer", opacity, 0)   
  g.gimp_drawable_fill(back_layer, 1)
  g.gimp_image_insert_layer(imageX, back_layer, None, 0)    #  
  g.gimp_layer_set_offsets(back_layer, 0, 0)    

  display = g.gimp_display_new(imageX)  
  fileextension = ".png"

  icon_layer = g.gimp_layer_new_from_drawable(tdrawable, imageX)
  g.gimp_image_insert_layer(imageX, icon_layer, None, 0)  
  g.gimp_layer_scale(icon_layer, icon_size, icon_size,FALSE)
  g.gimp_layer_set_offsets(icon_layer, (target_width-icon_size)/2, (target_height-icon_size)/2)  
  
  # Merge and save
  temp_layer = g.gimp_image_merge_visible_layers(imageX, 1)
  filename = "splash_" + str(target_width) + "x" + str(target_height) + fileextension
  filename = os.path.join(outputFolder,filename)  
  g.file_png_save(imageX, temp_layer, filename, "raw_filename", 0, 9, 0, 0, 0, 0, 0)
    
register(
  "python_intel_xdk_splash",
  "Intel XDK App Splash Screen Generator.",
  "Generate a set of app splash screens for use with the Intel XDK.",
  "Matt Hawkins",
  "Matt Hawkins",
  "2016",
  "<Image>/Filters/TechSpy/_App Splash Screens",
  "RGB*,GRAY*",
  [
      (PF_OPTION,  "platform", "Platform:", 0,["Android","iOS","Windows 10"]),
      (PF_SPINNER, "iconscale", "Icon Scale (%):", 50, (10, 100, 5)),
      (PF_TOGGLE,  "background_enable", "Background:", True),
      (PF_COLOR,   "background_color", "Background colour", (0, 0, 0)    ),
      (PF_DIRNAME, "outputFolder", "Output directory", "/")
  ], 
  [],
  python_intel_xdk_splash)

main()
