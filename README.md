# README #

This is a collection of Python scripts that can be added to Gimp to automate various tasks. They generally relate to web or app development.

* **mh_intel_xdk_launcher_icons.py**
Takes the currently open image and creates a set of scaled copies. These can be used as launcher icons in the Intel XDK build settings.

* **mh_intel_xdk_splash.py**
Takes the currently open image and creates a set of splash screen images of varous dimensions to be used in the Intel XDK build settings.

Please read this blog post for instructions on installing Python scripts in Gimp :
[Installing GIMP Plugins and Scripts In Windows](http://www.tech-spy.co.uk/2014/08/installing-gimp-plugins-and-scripts-in-windows/)